<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%news_rubric}}`.
 */
class m210126_050203_create_article_rubric_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%article_rubric}}', [
            'article_id' => $this->integer()->notNull(),
            'rubric_id' => $this->integer()->notNull(),
        ]);
        $this->addForeignKey('fk_article_id',
            '{{%article_rubric}}',
            'article_id',
            '{{%article}}',
            'id');
        $this->addForeignKey('fk_category_id',
            '{{%article_rubric}}',
            'rubric_id',
            '{{%rubric}}',
            'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%article_rubric}}');
    }
}
