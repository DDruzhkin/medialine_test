<?php

namespace app\api\v1\controllers;

use app\models\Rubric;
use yii\data\ActiveDataProvider;

class ArticleController extends BaseApiController
{
    public $modelClass = 'app\models\Article';

    public function actions()
    {
        $actions = parent::actions();
        $customActions = ['index'];
        array_walk($customActions, function ($action) use (&$actions) {
            unset($actions[$action]);
        });
        return $actions;
    }

    public function actionIndex()
    {
        return $this->modelClass::find()
            ->with('rubrics')
            ->all();
    }

    public function actionRubric($id)
    {
        $rootRubric = Rubric::find()
            ->where(['=', 'id', $id])
            ->one();
        $articles = $this->modelClass::find()
            ->joinWith(['rubrics' => function ($query) use ($rootRubric) {
                $query->where(['>=', 'rubric.lft', $rootRubric->lft])
                    ->andWhere(['<=', 'rubric.rgt', $rootRubric->rgt])
                    ->andWhere(['=', 'rubric.tree', $rootRubric->tree]);
            }]);
        return $articles->all();
    }
}