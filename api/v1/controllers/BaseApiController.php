<?php


namespace app\api\v1\controllers;


use yii\rest\ActiveController;
use yii\web\Response;

class BaseApiController extends ActiveController
{
    public function behaviors()
    {
        return [
            'contentNegotiator' =>
                [
                    'class' => 'yii\filters\ContentNegotiator',
                    'formats' => [
                        'application/json' => Response::FORMAT_JSON,
                    ]
                ]
        ];
    }
}