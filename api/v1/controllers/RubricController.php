<?php
namespace app\api\v1\controllers;

class RubricController extends BaseApiController
{
    public $modelClass = 'app\models\Rubric';

    public function actions()
    {
        $actions = parent::actions();
        $customActions=['index'];
        array_walk($customActions, function ($action) use (&$actions){
            unset($actions[$action]);
        });
        return $actions;
    }

    public function actionIndex(){
        return $this->modelClass::buildTree();
    }
}