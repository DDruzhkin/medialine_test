<?php


namespace app\models;


use yii\db\ActiveRecord;

class Article extends ActiveRecord
{
    public static function tableName()
    {
        return '{{article}}';
    }

    public function fields()
    {
        return [
            'id',
            'title',
            'body',
            'rubrics'
        ];
    }

    public function rules()
    {
        return [
            [['title', 'body'], 'required'],
            [['title'], 'string', 'max' => 255],
            [['body'], 'string'],
        ];
    }

    public function getRubrics()
    {
        return $this->hasMany(Rubric::class, ['id' => 'rubric_id'])
            ->viaTable('article_rubric', ['article_id' => 'id']);
    }
}