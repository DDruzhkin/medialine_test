<?php


namespace app\models;


use creocoder\nestedsets\NestedSetsBehavior;
use yii\db\ActiveRecord;

class Rubric extends ActiveRecord
{
    public function behaviors()
    {
        return [
            'tree' => [
                'class' => NestedSetsBehavior::className(),
                'treeAttribute' => 'tree',
            ],
        ];
    }

    public function fields()
    {
        return [
            'id',
            'name',
        ];
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    public function getArticles()
    {
        return $this->hasMany(Article::class, ['id' => 'article_id'])
            ->viaTable('article_rubric', ['rubric_id' => 'id']);
    }

    public static function getRoots($tree_id = null)
    {
        $conditions = $tree_id ? ['tree' => $tree_id] : ['depth' => 0];
        return static::find()
            ->where($conditions)
            ->asArray()
            ->all();
    }

    public static function getTreeNodes($tree, $lft, $rgt)
    {
        return static::find()
            ->where(['=', 'tree', $tree])
            ->andWhere(['<=', 'rgt', $rgt])
            ->andWhere(['>=', 'lft', $lft])
            ->asArray()
            ->all();
    }

    public static function buildTree($tree_id = null)
    {
        $roots = static::getRoots();
        $result = [];
        foreach ($roots as $treeRoot) {
            $result = array_merge($result, static::nestify(
                static::getTreeNodes($treeRoot['tree'],
                    $treeRoot['lft'],
                    $treeRoot['rgt']
                ),
                ['id', 'name']
            )
            );
        }
        return $result;
    }

    public function nestify($set, $fields = [])
    {
        $trees = [];
        if (count($set) > 0) {
            $stack = [];
            foreach ($set as $node) {
                if (!empty($fields)) {
                    $item['depth'] = $node['depth'];
                    foreach ($fields as $field) {
                        if (isset($node[$field]))
                            $item[$field] = $node[$field];
                    }
                } else {
                    $item = $node;
                }
                $item['children'] = [];
                $l = count($stack);
                while ($l > 0 && $stack[$l - 1]['depth'] >= $item['depth']) {
                    array_pop($stack);
                    $l--;
                }
                if ($l == 0) {
                    $i = count($trees);
                    $trees[$i] = $item;
                    $stack[] = &$trees[$i];
                } else {
                    $i = count($stack[$l - 1]['children']);
                    $stack[$l - 1]['children'][$i] = $item;
                    $stack[] = &$stack[$l - 1]['children'][$i];
                }
            }
        }
        return $trees;
    }
}